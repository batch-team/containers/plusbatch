local kap = import "lib/kapitan.libjsonnet";
local inventory = kap.inventory();

{
  ['Dockerfile']: kap.jinja2_template("templates/Dockerfile.j2", inventory.parameters)
}
