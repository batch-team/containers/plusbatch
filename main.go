package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"

	log "github.com/sirupsen/logrus"
)

func getDatafromPuppetDB(args ...string) ([]map[string]interface{}, error) {
	log.Printf("ai-pdb %s", strings.Join(args, " "))
	out, err := exec.Command("ai-pdb", args...).Output()
	if err != nil {
		log.Fatal(err)
	}

	var data []map[string]interface{}
	err = json.Unmarshal(out, &data)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return data, nil
}

func queryPuppetDB(endpoint string, query string) ([]map[string]interface{}, error) {
	return getDatafromPuppetDB("raw", endpoint, "--query", query)
}

func queryPuppetDBWithLimit(endpoint string, query string, limit uint) ([]map[string]interface{}, error) {
	return getDatafromPuppetDB("raw", endpoint, "--query", query, "--limit", fmt.Sprintf("%d", limit))
}

func getPackagesByTag(node string, tag string) (string, error) {
	query := fmt.Sprintf(`["and", ["=", "certname", %q], ["=", "tag", %q]]`, node, tag)

	data, err := queryPuppetDB("/pdb/query/v4/resources/Package", query)
	if err != nil {
		return "", err
	}

	pkglist := []string{}
	for _, resource := range data {
		pkg := resource["title"].(string)
		pkglist = append(pkglist, pkg)
	}

	return strings.Join(pkglist[:], "\n"), nil
}

func getNodeByOperatingSystem(hostgroup string, release string) (string, error) {
	query := fmt.Sprintf(`["and", ["=", ["fact", "operatingsystemmajrelease"], %q], ["=", ["fact", "hostgroup"], %q]]`, release, hostgroup)
	data, err := queryPuppetDBWithLimit("/pdb/query/v4/nodes", query, 1)
	if err != nil {
		return "", err
	}
	return data[0]["certname"].(string), nil
}

func main() {
	log.Print("Fetching data from PuppetDB")

	el7node, _ := getNodeByOperatingSystem("bi/condor/gridworker/share/mixed", "7")
	el8node, _ := getNodeByOperatingSystem("lxplus/nodes/login", "8")
	el9node, _ := getNodeByOperatingSystem("bi/condor/gridworker/share/mixed", "9")
	el7pkgs, _ := getPackagesByTag(el7node, "bagplus::variants::batch")
	el8pkgs, _ := getPackagesByTag(el8node, "bagplus::variants::plusbatch")
	el9pkgs, _ := getPackagesByTag(el9node, "bagplus::variants::batch")

	log.Print("Writing data to files")

	err := ioutil.WriteFile("files/pkgs/el7/batch.txt", []byte(el7pkgs), 0644)
	if err != nil {
		log.Fatal(err)
	}
	log.Print("Package data written to files/pkgs/el7/batch.txt")

	err = ioutil.WriteFile("files/pkgs/el8/batch.txt", []byte(el8pkgs), 0644)
	if err != nil {
		log.Fatal(err)
	}
	log.Print("Package data written to files/pkgs/el8/batch.txt")

	err = ioutil.WriteFile("files/pkgs/el9/batch.txt", []byte(el9pkgs), 0644)
	if err != nil {
		log.Fatal(err)
	}
	log.Print("Package data written to files/pkgs/el9/batch.txt")
}
