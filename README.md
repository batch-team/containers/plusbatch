# Plusbatch

Plusbatch generates the dockerfiles for the software packages available in batch/lxplus for EL 7, EL 8 and EL 9 operating systems. It then builds the docker images and then publishes them [here](https://gitlab.cern.ch/batch-team/containers/plusbatch/container_registry).

Plusbatch can be used to obtain dockerfiles for different supported Operating systems with all the supported packages or with a custom list of packages. The software definitions for all the kapitan features are obtained from the [bagplus module](https://gitlab.cern.ch/ai/it-puppet-module-bagplus/). 

Please refer the official docs available [here](https://kapitan.dev/kapitan_overview/) to learn about the Kapitan framework.


## Generating dockerfiles

Plusbatch generates dockerfiles that correspond to the config specified in the `targets` directory. The following targets are available as default -

1. el7 -
    - full - Generates dockerfile that would comprise of all the supported software packages for CentOS 7 and their associated config files/scripts.
    - submit - Generates dockerfile that would comprise of only the specified supported software packages for CentOS 7 and their associated config files/scripts.
2. el8 -
    - full - Generates dockerfile that would comprise of all the supported software packages for CentOS 8 Stream and their associated config files/scripts.
    - submit - Generates dockerfile that would comprise of only the specified supported software packages for CentOS 8 and their associated config files/scripts.
3. slc6 -
    - full - Generates dockerfile that would comprise of all the supported software packages for SLC 6 and their associated config files/scripts.
    - submit - Generates dockerfile that would comprise of only the specified supported software packages for SLC 6 and their associated config files/scripts.

For generating dockerfiles that would comprise of only the user specified software packages -

1. Browse to the folder inside the `targets` directory which corresponds to your choice of OS. for eg, for EL 7, the directory would be `el7-submit`.
2. List your packages with their feature names under the `parameters/submit` parameter in the submit yml file. for eg, for CentOS 7, the fle would be `el7-submit.yml`.
3. Run `kapitan compile`
4. Browse to the `compiled` directory, then find your dockerfile along with the needed config files inside the directory that corresponds to your OS. for eg, for CentOS 7, the directory would be `el7-submit/`.


## Available template parameters
| Parameter        | Type           | Description  |
| ------------- |-------------|-----|
| name      | string     | Name of the feature/package |
| image     | string     | Docker image of the OS |
| os        | string     | Number for the OS release (RHEL 6, 7, 8 are supported as of now) |
| pkgset    | dictionary | List of pkgs that needs to be installed for a particular feature. `key` (string): OS release number or the string `common`(string) if a particular package is common for all the supported OS's, `value` (string): list of packages | 
| arg       | dictionary | Arguments that is passed to yum while installing packages. `key` (string): OS release number, `value` (string): dictionary of yum's argument key and value  |
| chown | list | `user`(string): unix username, `group`(string): unix group in which the user belongs, `path`: path of the file, `os`: OS release number (6/7/8)|
| chmod |list | `permission`(string): unix user permission code, `path`(string): path of the file, `os` ([]string): list of supported OS's |
| scripts | dictionary | `key`(string): OS release number, `value` (string): bash script |
| file | list | Copies the files from the source path to the target path. `source`(string): module file path, `destination`(string); , `os`([]string): list of supported OS's |
| pkgfile | dictionary | `key`(number): OS release number (6/7/8), `value`(string): path of the text file which ontains the list of packages |


## Adding a new feature from Bagplus

All the Bagplus features that are currently supported by this module are listed [here](https://gitlab.cern.ch/batch-team/containers/plusbatch/-/tree/master/inventory/classes/features). For adding a new feature from bagplus follow these instructions -

1.  Investigate bagplus module to prepare the installation procedure for that particular feature. The installation procedure for Bagplus features generally consist of these steps:
    -   Adding repositories in your OS
    -   Installing a list of packages
    -   Creating users, groups and giving them access permissions
    -   Adding configuration files in your system
    -   Running scripts
2.  Create a new file `[feature-name].yml` under the path `inventory/classes/features`. Then code the installation procedure for the feature using the [available template parameters](https://gitlab.cern.ch/batch-team/containers/plusbatch/-/tree/master#quick-start). 
3.  Store the required configuration files for the feature in the `files/` directory. Keep the filepath inside the `files` directory same as the destination filepath just to maintain uniformity.
4.  Include the file `[feature-name].yml` in the [inventory classes](https://gitlab.cern.ch/batch-team/containers/plusbatch/-/tree/master/inventory/classes) for all the OS's that it's supported by. for eg, for adding a feature in CentOS7 and CentOS8 you'd need to include the feature yml file in `inventory/classes/el7-dockerfile.yml` and `inventory/classes/cc8-dockerfile.yml`
5. Write appropiate goss tests under `tests/EL(7/8/9)/goss.yaml`

## Releasing docker images

New docker images are released every week through a scheduled gitlab CI pipeline. This release pipeline comprises of the following stages which are run sequentially, only after one stage completes sucessfully the other start execution. Each stage comprises of a job for each of the plusbatch target (ex. el8-full, el7-full, el7-submit etc) which are run in parallel. The pipeline for every target is independent of the pipeline for any other target.

1.  The first CI stage build the docker images from the Dockerfiles present in the compiled/ directory in the project's master branch. It then push these images to the Gitlab Registery with the current date tag - `YYYYMMDD` and the `test` tag.
2.  This CI stage pulls the docker images that were tagged with the `test` tag. Then it validates the server configuration (which is comprised of the following things) using Goss -
    *   The required packages are installed
    *   The required configuration files along with their user permissions etc are present
    *   The required services are present/running
3.  This CI stage replaces the `test` tag with the `QA` tag for all those docker images that pass sucessfully in the previous stage of goss testing.
4.  This CI stage releases the docker images previously tagged as `QA` with the `latest` tag. Note that this stage is manual i.e. it starts execution only after the maintainer presses the start button.


## Automatic updation of batch pkgset

The Go script available [here](https://gitlab.cern.ch/batch-team/containers/plusbatch/-/blob/master/main.go) queries hosts running el7, el8 and el9 for the installed software packages having the puppet tag `bagplus::variants::batch`. It then writes the scraped names inside the directory `files/pkgs/[os]`. These files are then picked up Kapitan to populate Dockerfiles for respective operating systems when the project is compiled.


## Automatic generation of updated dockerfiles

The dockerfiles and the associated config files consisted with the current configuration is available in the `compiled/` directory. The CI job titled `update-compiled` runs daily to update the compiled directory with the latest configuration changes in the project that might have been introduced through a recent merge. This CI job then sends a merge request to commit the latest changes in the compiled directory to the project which can then be merged into the source tree after the maintainer's approval.
